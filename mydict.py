import os
from collections import Counter

from norvig.pytudes.py import spell

dict_set = set()

class Dictionary:

    def load_dict(self):
        cur_dir = os.path.dirname(__file__)
        path = os.path.join(cur_dir, 'scowl_word_lists/final')
        for filename in os.listdir(path):
            with open(os.path.join(path, filename), 'rb') as fp:
                for line in fp:
                    dict_set.add(line.decode('ascii','ignore').strip('\n').lower())
        spell.WORDS = Counter(list(dict_set))

    def find_word(self, word):
        return word in dict_set

    def get_suggestions(self, word):
        if(word in dict_set):
            return [word]
        return spell.candidates(word)
