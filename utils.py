import os
import re

from bs4 import BeautifulSoup

from mydict import Dictionary
from norvig.pytudes.py import spell

tags = ['p', 'h1', 'h2', 'h3', 'h4', 'h5']

class Util:

    def __init__(self):
        self.dictionary = Dictionary()
        self.dictionary.load_dict()
        self.dir = '%s/output' % os.getcwd()
        self.errors_dict = {}

    def get_text(self):
    
        for filename in os.listdir(self.dir):
            f = open(self.dir+'/'+filename, 'r')
            soup = BeautifulSoup(f.read(), "html.parser")
            for tag in soup.find_all(tags):
                self.check_tag(BeautifulSoup(str(tag), "html.parser").get_text().strip())

        return self.output()

    def check_tag(self, tag_text):
        wlist = spell.words(tag_text)
        for word in wlist:
            if not self.dictionary.find_word(word):
                self.errors_dict[word] = str(self.dictionary.get_suggestions(word))

    def output(self):
        found = False
        i = 0
        ret = '''<table style="width:100%">
                   <tr>
                     <th style="text-align:center;">Errors</th>
                     <th style="text-align:center;">Suggestions</th>
                     <th style="text-align:center;">Page</th>
                     <th style="text-align:center;">Line Number</th>
                  </tr>'''

        for filename in os.listdir(self.dir):
            with open(self.dir+'/'+filename, 'r') as f:
                for line in f:
                    i = i + 1
                    for key in self.errors_dict.keys():
                        if key in line:
                            ret = ret + '''<tr>
                                             <td>{}</td>
                                             <td>{}</td>
                                             <td>{}</td>
                                             <td>{}</td>
                                           </tr>'''.format(key, self.errors_dict[key],
                                                           self.filename_to_link(filename), i)
        return ret + "</table>"

    def filename_to_link(self, filename):
        return filename.strip('__').strip('.txt').replace('_', '/')
