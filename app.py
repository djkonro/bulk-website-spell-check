import subprocess

from flask import Flask, request
from flask import render_template

from utils import Util
import scraper

app = Flask(__name__)
 
@app.route("/")
def index():
    return render_template('index.html')

@app.route('/result', methods=['GET', 'POST'])
def result():
    url = request.form.get('url')
    scraper.URL = url
    s = scraper.Scraper()
    s.run()
    u = Util()
    return render_template('result.html', data=u.get_text())

if __name__ == "__main__":
    app.run()
