import os

from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

class MySpider(CrawlSpider):
    name = 'spider'

    def __init__(self, url='', **kwargs):
        super(MySpider, self).__init__(**kwargs)
        self.allowed_domains = [url]
        self.start_urls = ['http://'+url]

    rules = (
        Rule(LinkExtractor(), callback='parse_item', follow=True),
    )

    def parse_start_url(self, response):
        return self.parse_item(response)

    def parse_item(self, response):
        page = response.url.split(":")[1].replace('/', '_')
        filename = 'output/%s.txt' % page
        if not os.path.exists('output'):
            os.makedirs('output')
        with open(filename, 'w') as f:
            f.write(response.body.decode('utf-8'))
        self.log('Saved file %s' % filename)

